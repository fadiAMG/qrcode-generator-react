import React, { Component } from "react";
import QRCode from "qrcode";


const styles = {
  block: {
    width: '1050px',
    margin: '0 auto',

  }
};

export class QrCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urls: ["google.com", "youtube.com", "facebook.com", "foo.com" , "google.com", "youtube.com", "facebook.com", "foo.com"],
      urlCodes: []
    };
  }
  componentDidMount() {
    this.generateQrCodes();
  }

  fetchWorkshopsUrls() {}

  generateQrCodes() {
    this.state.urls.map(url => {
      QRCode.toDataURL(`${url}`, { errorCorrectionLevel: "H" })
        .then(url => {
          //   console.log(url);
          this.setState({ urlCodes: [...this.state.urlCodes, url] });
        })
        .catch(err => {
          console.error(err);
        });
    });
  }
  render() {
    let _self = this;

    function renderCodes() {
      return (
        <div>
          {_self.state.urlCodes.map((code, i) => {
            return (
              <div style={styles.block}>
                <img
                style = {{width: '900px'}}
                  src={code}
                  alt="Workshop QR-CODE"
                />
                <h2>
                  Workshop {i}
                </h2>
              </div>
            );
          })}
        </div>
      );
    }

    return (
      <div>

        <div
          style={{
            backgroundColor: "#f5f5f5",
            width: "297mm",
            height: "100%",
            margin: 'auto'
          }}
        >
          <h1>WORKSHOP QRCODES</h1>
          {renderCodes()}
        </div>
      </div>
    );
  }
}

export default QrCode;
